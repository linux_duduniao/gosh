package config

import (
	"os"
	"path"
)

var GlobalConfig Config

type Config struct {
	UsedDB bool
	DBFile string
}

func init() {
	if os.Getenv("GOSH_DB_USED") == "true" {
		GlobalConfig.UsedDB = true
	}
	if os.Getenv("GOSH_DB_FILE") == "" {
		GlobalConfig.DBFile = path.Join(os.Getenv("HOME"), ".config/gosh.db")
	} else {
		GlobalConfig.DBFile = os.Getenv("GOSH_DB_FILE")
	}
}
