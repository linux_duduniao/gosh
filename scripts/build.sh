#!/usr/bin/env bash

export GO111MODULE=on
export GOARCH=amd64
export GOPROXY=https://goproxy.cn,direct
export CGO_ENABLED=0

version=$(go run main.go -v|awk '{print $NF}')
pkg_dir=/tmp/gosh-"${version}"

# shellcheck disable=SC1073
for os in linux windows darwin
do
  [ $os == "windows" ] && binary=gosh.exe || binary=gosh
  GOOS=$os go build -o "$pkg_dir"/$binary && echo "build $binary for $os succeed"
  cd "$pkg_dir" && tar -zcf gosh-${os}-amd64-"${version}".tar.gz $binary && rm -f $binary && cd - >/dev/null || exit
done

