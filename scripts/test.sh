#!/usr/bin/env bash

PREPARE() {
  go build -o /tmp/gosh main.go || return 1
  echo 10.10.10.101 > /tmp/dest-single.ip
  echo -e "10.10.10.101\n10.10.10.102\n10.10.10.103\n10.10.10.104" > /tmp/dest-multi.ip
  mkdir -p ~/.ssh 2>/dev/null ; chmod 700 ~/.ssh
  [ -d scripts ] && cd scripts || return  1
  cp ssh_keys/dev/id_rsa ~/.ssh/id_rsa && chmod 400 ~/.ssh/id_rsa || return 1
  docker compose -f docker-compose.yaml up -d && sleep 10 || return 1
}

CMD_Single_Without_Bastion(){
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh cmd -H 10.10.10.101 -u dev -k ssh_keys/dev/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-single.ip -u dev -k ssh_keys/dev/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-single.ip -u dev --print.status failed id )
  [ "$res" == "" ] || return 1
}

CMD_Single_With_Bastion(){
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh cmd -H 10.10.10.101 -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-single.ip -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-single.ip -u dev --bastion.host 10.10.10.100 --bastion.user dev --print.status failed id )
  [ "$res" == "" ] || return 1
}

CMD_Multi_Without_Bastion() {
  # shellcheck disable=SC2155
  local res=$( /tmp/gosh cmd -H "10.10.10.101,10.10.10.102,10.10.10.103" -u dev -k ssh_keys/dev/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-multi.ip -u dev -k ssh_keys/dev/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-multi.ip -u dev --print.status failed id )
  [ "$res" == "" ] || return 1
}


CMD_Multi_With_Bastion(){
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh cmd -H "10.10.10.101,10.10.10.102,10.10.10.103" -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-multi.ip -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed id )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh cmd -i /tmp/dest-multi.ip -u dev --bastion.host 10.10.10.100 --bastion.user dev --print.status failed id )
  [ "$res" == "" ] || return 1
}

Push_Single_Without_Bastion(){
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh push -H 10.10.10.101 -u dev -k ssh_keys/dev/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-single.ip -u dev -k ssh_keys/dev/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-single.ip -u dev --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1
}

Push_Single_With_Bastion(){
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh push -H 10.10.10.101 -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-single.ip -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-single.ip -u dev --bastion.host 10.10.10.100 --bastion.user dev --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1
}

Push_Multi_Without_Bastion(){
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh push -H "10.10.10.101,10.10.10.102,10.10.10.103" -u dev -k ssh_keys/dev/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-multi.ip -u dev -k ssh_keys/dev/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-multi.ip -u dev --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1
}

Push_Multi_With_Bastion(){
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh push -H "10.10.10.101,10.10.10.102,10.10.10.103" -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-multi.ip -u dev -k ssh_keys/dev/id_rsa --bastion.host 10.10.10.100 --bastion.user bastion --bastion.key ssh_keys/bastion/id_rsa --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1

  res=$(/tmp/gosh push -i /tmp/dest-multi.ip -u dev --bastion.host 10.10.10.100 --bastion.user dev --print.status failed ../images ../main.go /tmp )
  [ "$res" == "" ] || return 1
}

Push_Local_Proxy(){
  nohup /tmp/gosh proxy -H 10.10.10.101 -u dev -r 10.10.102:22 -l 0.0.0.0:2022 >/dev/null 2>&1
  # shellcheck disable=SC2155
  local res=$(/tmp/gosh cmd -H 127.0.0.1 -p 2022 -u dev -k ssh_keys/dev/id_rsa --print.status failed id)
  [ "$res" == "" ] && kill %1 || return 1
}

Push_Remote_Proxy(){
  :
}

# shellcheck disable=SC2034
for f in PREPARE CMD_Single_Without_Bastion CMD_Single_With_Bastion CMD_Multi_Without_Bastion CMD_Multi_With_Bastion \
  Push_Single_Without_Bastion Push_Single_With_Bastion  Push_Multi_Without_Bastion Push_Multi_With_Bastion \
  Push_Local_Proxy
do
    $f && continue
    docker compose -f docker-compose.yaml down
    exit 1
done




