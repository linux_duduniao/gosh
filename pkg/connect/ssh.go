package connect

import (
	"golang.org/x/crypto/ssh"
)

// sshClientConfig 生成密钥信息, 分为两种：password 和 ssh key
func sshClientConfig(passwd, key, passphrase string) (auth []ssh.AuthMethod, err error) {
	if passwd != "" {
		auth = append(auth, ssh.Password(passwd))
	}
	if key != "" {
		var singer ssh.Signer
		if passphrase != "" {
			singer, err = ssh.ParsePrivateKeyWithPassphrase([]byte(key), []byte(passphrase))
		} else {
			singer, err = ssh.ParsePrivateKey([]byte(key))
		}
		if err != nil {
			return nil, err
		}
		auth = append(auth, ssh.PublicKeys(singer))
	}
	return
}
