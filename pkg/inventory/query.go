package inventory

import (
	"fmt"
	"github.com/fatih/color"
	"gosh/pkg/model"
)

func Query() {
	switch FilterConfig.Type {
	case "bastion":
		res, err := model.QueryBastionsForPrint(FilterConfig.Condition)
		if err != nil {
			color.Red("query bastion failed,error:%s", err.Error())
			return
		}
		fmt.Println(res)
	case "host":
		res, err := model.QueryHostsForPrint(FilterConfig.Condition)
		if err != nil {
			color.Red("query host failed,error:%s", err.Error())
			return
		}
		fmt.Println(res)
	}
}
