package inventory

import (
	"github.com/fatih/color"
	"github.com/xuri/excelize/v2"
	"gosh/pkg/model"
)

func ExportFromDB() {
	file := excelize.NewFile()
	var err error
	switch ExportConfig.Filter.Type {
	case "bastion":
		err = writeBastions(file, ExportConfig.Filter.Condition)
		if err != nil {
			color.Red("export bastion failed,err:%s", err.Error())
			return
		}
	case "host":
		err = writeHosts(file, ExportConfig.Filter.Condition)
		if err != nil {
			color.Red("export host failed,err:%s", err.Error())
			return
		}
	case "all":
		err = writeBastions(file, ExportConfig.Filter.Condition)
		if err != nil {
			color.Red("export bastion failed,err:%s", err.Error())
			return
		}
		err = writeHosts(file, ExportConfig.Filter.Condition)
		if err != nil {
			color.Red("export host failed,err:%s", err.Error())
			return
		}
	}
	_ = file.DeleteSheet("Sheet1")
	err = file.SaveAs(ExportConfig.Path)
	if err != nil {
		color.Red("save excel failed,err:%s", err.Error())
	}
}

func writeBastions(file *excelize.File, conditions map[string]string) error {
	_, err := file.NewSheet("bastion")
	if err != nil {
		return err
	}
	_ = file.SetColWidth("bastion", "A", "C", 15)
	_ = file.SetColWidth("bastion", "F", "F", 15)
	_ = file.SetColWidth("bastion", "G", "G", 40)
	_ = file.SetColWidth("bastion", "H", "J", 19)
	err = file.SetSheetRow("bastion", "A1", &[]interface{}{"connect_ip", "private_ip", "public_ip", "ssh_port", "ssh_user", "ssh_password", "ssh_key_file", "ssh_key_passphrase", "vendor", "region"})
	if err != nil {
		return err
	}
	bastions, err := model.QueryBastionsByConditions(conditions)
	if err != nil {
		return err
	}
	for index, bastion := range *bastions {
		cell, err := excelize.CoordinatesToCellName(1, index+2)
		if err != nil {
			return err
		}
		err = file.SetSheetRow("bastion", cell, &[]interface{}{bastion.ConnectIP, bastion.PrivateIP, bastion.PublicIP,
			bastion.SSHPort, bastion.SSHUser, bastion.SSHPassword, bastion.SSHKeyFile, bastion.SSHKeyPassphrase, bastion.Vendor, bastion.Region})
		if err != nil {
			return err
		}
	}
	return nil
}

func writeHosts(file *excelize.File, conditions map[string]string) error {
	_, err := file.NewSheet("host")
	if err != nil {
		return err
	}
	//_ = file.SetPanes("host", &excelize.Panes{Freeze: true, XSplit: 0, YSplit: 1})
	_ = file.SetColWidth("host", "A", "D", 15)
	_ = file.SetColWidth("host", "G", "G", 15)
	_ = file.SetColWidth("host", "H", "H", 40)
	_ = file.SetColWidth("host", "I", "I", 19)
	_ = file.SetColWidth("host", "J", "N", 19)

	err = file.SetSheetRow("host", "A1", &[]interface{}{"private_ip", "public_ip", "connect_ip", "bastion_ip", "ssh_port", "ssh_user", "ssh_password", "ssh_key_file", "ssh_key_passphrase", "vendor", "region", "project", "env", "app"})
	if err != nil {
		return err
	}
	hosts, err := model.QueryHostsByCondition(conditions)
	if err != nil {
		return err
	}
	for index, host := range *hosts {
		cell, err := excelize.CoordinatesToCellName(1, index+2)
		if err != nil {
			return err
		}
		err = file.SetSheetRow("host", cell, &[]interface{}{host.PrivateIP, host.PublicIP, host.ConnectIP, host.BastionIP,
			host.SSHPort, host.SSHUser, host.SSHPassword, host.SSHKeyFile, host.SSHKeyPassphrase, host.Vendor, host.Region, host.Project, host.Env, host.App})
		if err != nil {
			return err
		}
	}
	return nil
}
