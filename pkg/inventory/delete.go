package inventory

import (
	"github.com/fatih/color"
	"gosh/pkg/model"
)

func Delete() {
	switch FilterConfig.Type {
	case "bastion":
		err := model.DeleteBastionsByCondition(FilterConfig.Condition)
		if err != nil {
			color.Red("query bastion failed,error:%s", err.Error())
			return
		}
	case "host":
		err := model.DeleteHostsByCondition(FilterConfig.Condition)
		if err != nil {
			color.Red("query host failed,error:%s", err.Error())
			return
		}
	}

}
