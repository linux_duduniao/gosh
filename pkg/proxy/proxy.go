package proxy

import (
	"github.com/fatih/color"
	"gosh/pkg/connect"
	"os"
)

var jumpServer *connect.JumpServer

func Run() {
	initJumpServer()
	switch ProxyConfig.Type {
	case "local":
		localProxy()
	case "remote":
		remoteProxy()
	}
}

func initJumpServer() {
	if ProxyConfig.BastionHost != "" {
		jumpServer = connect.NewJumpServer(ProxyConfig.BastionHost, ProxyConfig.BastionUser, ProxyConfig.Password,
			ProxyConfig.BastionKeyContent, ProxyConfig.BastionKeyPassphrase, ProxyConfig.BastionPort, ProxyConfig.ConnectTimeout)
		err := jumpServer.Open()
		if err != nil {
			color.Red("connect to bastion %s@%s failed,err:%s", jumpServer.Username, jumpServer.IP, err.Error())
			os.Exit(1)
		}
	}
}
