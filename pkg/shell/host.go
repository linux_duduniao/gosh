package shell

import (
	"time"
)

type Host struct {
	ConnectIP        string
	BastionIP        string
	SSHPort          int
	SSHUser          string
	SSHPassword      string
	SSHKeyContent    string
	SSHKeyFile       string
	SSHKeyPassphrase string
	SSHTimeout       time.Duration
}
