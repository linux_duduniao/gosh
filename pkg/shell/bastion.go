package shell

import (
	"gosh/pkg/connect"
	"time"
)

type Bastion struct {
	IP               string
	SSHPort          int
	SSHUser          string
	SSHPassword      string
	SSHKeyFile       string
	SSHKeyContent    string
	SSHKeyPassphrase string
	SSHTimeout       time.Duration
}

var jumpserverMap = make(map[string]*connect.JumpServer)

func initJumpServer() error {
	for ip, bastion := range bastionMap {
		jumpServer := connect.NewJumpServer(bastion.IP, bastion.SSHUser, bastion.SSHPassword, bastion.SSHKeyContent,
			bastion.SSHKeyPassphrase, bastion.SSHPort, bastion.SSHTimeout)
		if err := jumpServer.Open(); err != nil {
			return err
		}
		jumpserverMap[ip] = jumpServer
	}
	return nil
}
