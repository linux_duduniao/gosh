package handler

type result struct {
	ip           string
	stdout       []byte
	stderr       []byte
	status       bool
	printIP      bool
	printStdout  bool
	printStderr  bool
	printFailed  bool
	printSucceed bool
}

func newResult(ip string, printStatus []string, printFields []string) *result {
	res := &result{
		ip:     ip,
		stdout: make([]byte, 0, 1024),
		stderr: make([]byte, 0, 1024),
		status: false,
	}

	for _, output := range printFields {
		switch output {
		case "ip":
			res.printIP = true
		case "stderr":
			res.printStderr = true
		case "stdout":
			res.printStdout = true
		}
	}

	for _, status := range printStatus {
		switch status {
		case "failed":
			res.printFailed = true
		case "succeed":
			res.printSucceed = true
		}
	}
	return res
}
