package handler

import "sync"

var (
	ipChan  chan string
	resChan chan *result
	wg      sync.WaitGroup
)

func initChan(fork int) {
	ipChan = make(chan string, fork)
	resChan = make(chan *result, fork)
}
