# 0. 介绍
Gosh 是由golang编写的集成运维常用功能的小工具，基于ssh协议完成远程主机批量执行命令、下发文件、ssh端口转发功能。
```text
[root@duduniao ~]# gosh --help
基于ssh协议的批处理工具

Usage:
  gosh [command]

Available Commands:
  cmd         连接远程服务器并执行shell命令
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  inventory   操作本地主机信息数据库
  login       使用ssh协议登录远程服务器
  proxy       将远程服务TCP端口代理到本地
  pull        批量拉取远程主机文件到本地
  push        批量发送文件到远程主机

Flags:
  -h, --help      help for gosh
  -v, --version   version for gosh

Use "gosh [command] --help" for more information about a command.
```
---
# 1. 配置命令补全功能
```text
[root@duduniao ~]# gosh completion
Generate the autocompletion script for gosh for the specified shell.
See each sub-command's help for details on how to use the generated script.

Usage:
  gosh completion [command]

Available Commands:
  bash        Generate the autocompletion script for bash
  fish        Generate the autocompletion script for fish
  powershell  Generate the autocompletion script for powershell
  zsh         Generate the autocompletion script for zsh

Flags:
  -h, --help   help for completion
```
以Linux的Bash为例：
```text
# 将生成的补全脚本放入 /etc/bash_completion.d 下
[root@duduniao ~]# gosh completion bash > /etc/bash_completion.d/gosh

# 退出当前shell后，重新打开终端，可以使用 tab 完成命令补全
[root@duduniao ~]# gosh <TAB>
cmd         (连接远程服务器并执行shell命令)                                   login       (使用ssh协议登录远程服务器)
completion  (Generate the autocompletion script for the specified shell)  proxy       (将远程服务TCP端口代理到本地)
help        (Help about any command)                                      pull        (批量拉取远程主机文件到本地)
inventory   (操作本地主机信息数据库)                                          push        (批量发送文件到远程主机)
```
---
# 2. Gosh 本地数据库操作
Gosh 为了管理复杂多地区环境，Gosh使用本地 sqlite3 数据库来存储服务器信息，并且提供增删改查功能，用户可以根据需求每天从CMDB同步到本地 <br />
为了使用gosh数据库功能，需要设置两个环境变量，可以将其写入到 ~/.bash_profile 
```text
export GOSH_DB_USED=true
export GOSH_DB_FILE=~/.config/gosh.db
```

```text
[root@duduniao ~]# gosh inventory --help
操作本地主机信息数据库

Usage:
  gosh inventory [flags]
  gosh inventory [command]

Available Commands:
  delete      批量删除数据库中的主机数据
  export      根据条件导出本地主机信息
  get         根据条件查询本地主机信息
  import      批量导入主机和跳板机信息，如果数据库存在则更新，不存在则创建

Flags:
  -h, --help   help for inventory

Use "gosh inventory [command] --help" for more information about a command
```

## 2.1. 数据导入
Gosh 支持从 .xlsx 格式文件导入数据
```text
[root@duduniao ~]# gosh inventory import --help
执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

自动读取名为 bastion 的sheet作为 bastion 数据源，并跳过第一行(默认为title)
自动读取名为 host 的sheet作为 host 数据源，并跳过第一行(默认为title)

Examples:
# 从excel文件导入跳板机和服务器信息
gosh inventory -f /mnt/c/Users/test/Downloads/gosh.xlsx

Usage:
  gosh inventory import -f <path> [flags]

Flags:
  -f, --file string   导入的文件
  -h, --help          help for import
```
## 2.2. 数据查询
```text
[root@duduniao ~]# gosh inventory get --help
执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

Note: 因为考虑到性能问题，不支持 模糊匹配, 取反等操作

Examples:
# 查询全部跳板机信息:
~ gosh inventory get -t bastion

# 查询腾讯上海跳板机信息:
~ gosh inventory get -t bastion -l vendor=tencent -l region=shanghai

# 查询指定IP的跳板机
~ gosh inventory get -t bastion -l private_ip=127.0.0.1

Usage:
  gosh inventory get [-t host|bastion] [-l k1=v1] [-l k2=v2] ...  [flags]

Flags:
  -h, --help            help for get
  -l, --label strings   查询条件
  -t, --type string     导入的数据类型,支持bastion和host (default "host")
```
## 2.3. 数据删除
```text
[root@duduniao ~]# gosh inventory delete --help
执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

Note: 因为考虑到性能问题，不支持 模糊匹配。为了避免误操作清空数据库，要求必须填写条件

Examples:

# 删除腾讯上海的服务器:
~ gosh inventory delete -t host -l region=shanghai -l vendor=tencent

# 删除指定IP的跳板机
~ gosh inventory delete -t bastion -l private_ip=127.0.0.1

Usage:
  gosh inventory delete [-t host|bastion] [-l k1=v1] [-l k2=v2] [flags]

Flags:
  -h, --help            help for delete
  -l, --label strings   查询条件
  -t, --type string     删除的数据类型,支持bastion和host (default "host")
```
## 2.4. 数据导出
```text
[root@duduniao ~]# gosh inventory export --help
执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

Note: 因为考虑到性能问题，不支持 模糊匹配, 取反等操作。
      当主机信息发生变更时，可以先导入这部分机器，然后在 excel 中修改完毕后再导入

Examples:
# 导出全部信息:
~ gosh inventory export -f res.xlsx

# 导出腾讯上海主机信息:
~ gosh inventory export -t host -l vendor=tencent -l region=shanghai -f res.xlsx

# 查询指定IP的跳板机
~ gosh inventory export -t bastion -l private_ip=127.0.0.1 -f res.xlsx

Usage:
  gosh inventory export -f <path> [-t host|bastion|all] [-l k1=v1] [-l k2=v2] ... [flags]

Flags:
  -f, --file string     导出的文件路径 (default "gosh.xlsx")
  -h, --help            help for export
  -l, --label strings   查询条件
  -t, --type string     导入的数据类型,支持bastion,host,all (default "all")
```
---
# 3. 批量执行shell命令
Gosh 支持指定跳板机，但是目前只支持一个跳板机，暂时不支持多级跳板机操作
```text
[root@duduniao ~]# gosh cmd --help
批量连接远程服务器并执行shell命令，最多支持使用一层跳板机
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提:
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配

Examples:
# 输出无法连通的机器IP地址
~ gosh cmd -i ip.txt -u ops -p 2333 --print.status failed --print.field ip "id"

# 读取 ip.txt 文件中的列表，并检查主机名, 提升至30并发:
~ gosh cmd -i ip.txt -f 30 "hostname" | xargs -n 2 | column -t

# 检查服务状态
~ gosh cmd -i ip.txt "sudo systemctl is-active firewalld.service; sudo systemctl is-enabled firewalld.service"| xargs -n 3 | column -t

# 远程执行脚本，并打印执行失败的主机执行信息
~ gosh cmd -i ip.txt --print.status failed "sudo bash /tmp/install.sh"

# 读取 ip.txt 文件中的列表，并远程扫描 /data 数据目录大小:
~ gosh cmd -i ip.txt -u ops -p 2333 -k ~/.ssh/k1 --bastion.host 10.100.100.100 "df -h /data/ | awk 'NR==2{print \$2}'"

# 从命令行直接读取IP列表，可以使用 echo 'xxx xxx xxx' | xargs | tr " " "," 生成列表
~ gosh cmd -H 127.0.0.1,127.0.0.2,127.0.0.3,127.0.0.4,127.0.0.5 "grep '^#PermitRootLogin' /etc/ssh/sshd_config"

Usage:
  gosh cmd {-i hosts.txt|-H host01,host02,...} [flags] commands...

Flags:
      --bastion.host string             指定跳板机IP
      --bastion.key string              指定跳板机ssh私钥
      --bastion.key.passphrase string   指定跳板机ssh私钥的密码
      --bastion.password string         指定跳板机密码
      --bastion.port int                指定跳板机端口
      --bastion.user string             指定跳板机用户,默认当前用户
  -t, --connect.timeout duration        ssh连接超时时间 (default 5s)
  -f, --fork int                        指定并发数量 (default 10)
  -h, --help                            help for cmd
  -H, --host string                     指定主机列表，使用逗号分隔
  -i, --inventory string                指定主机清单列表文件,# 开头为注释
  -k, --key string                      指定ssh私钥
      --key.passphrase string           ssh私钥的密码
  -l, --label strings                   指定标签选择器
  -P, --password string                 远程ssh用户的密码
  -p, --port int                        指定远程主机ssh的端口
      --print.field strings             选择打印的消息字段 (default [ip,stdout,stderr])
      --print.status strings            选择打印成功或者失败的消息 (default [failed,succeed])
  -u, --user string                     远程ssh用户,默认当前用户
```
---
# 4. 批量下发文件
Gosh支持基于sftp协议下发多个文件或者目录到远程主机，虽然支持下发目录，但是对于有特殊权限的目录，建议打包后下发到目标主机 <br />
```text
[root@duduniao ~]# gosh push --help
批量发送文件到远程主机，最多支持使用一层跳板机。
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提:
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配
   虚拟机支持的筛选字段: vendor,region,private_ip,public_ip,user,port,key,bastion,connect_ip
4. push 命令传输速度很慢，不适合传输大文件，速度远低于 scp 命令
5. 将本地目录传输到远程时，如果本地目录以 / 结尾，则拷贝目录下子文件

Examples:
# 读取 ip.txt 文件中的列表，下发系统参数配置到远程主机上:
~ gosh push -i ip.txt ops-sysctl.conf /etc/sysctl.d/

# 读取 ip.txt 文件中的列表，并推送nginx配置到远程主机:
~ gosh push -i ip.txt -u ops -p 2333 -k ~/.ssh/k1 --bastion.host 10.100.100.100 nginx.conf conf.d ssl /etc/nginx/

# 读取 ip.txt 文件中的列表，并推送压缩文件到远程主机:
~ gosh push -i ip.txt -u ops -p 2333 tarball/*.tar.gz /opt/src/

# 读取 ip.txt 文件中的列表，并推送filebeat目录下配置文件到远程主机:
~ gosh push -i ip.txt -u ops -p 2333 config/filebeat/ /opt/apps/filebeat/inputs.d/

Usage:
  gosh push {-i hosts.txt|-H host01,host02,...} [flags] localFile... remoteDirectory

Flags:
      --bastion.host string             指定跳板机IP
      --bastion.key string              指定跳板机ssh私钥
      --bastion.key.passphrase string   指定跳板机ssh私钥的密码
      --bastion.password string         指定跳板机密码
      --bastion.port int                指定跳板机端口
      --bastion.user string             指定跳板机用户,默认当前用户
  -t, --connect.timeout duration        ssh连接超时时间 (default 5s)
  -f, --fork int                        指定并发数量 (default 10)
  -h, --help                            help for push
  -H, --host string                     指定主机列表，使用逗号分隔
  -i, --inventory string                指定主机清单列表文件,# 开头为注释
  -k, --key string                      指定ssh私钥
      --key.passphrase string           ssh私钥的密码
  -l, --label strings                   指定标签选择器
  -P, --password string                 远程ssh用户的密码
  -p, --port int                        指定远程主机ssh的端口
      --print.field strings             选择打印的消息字段 (default [ip,stdout,stderr])
      --print.status strings            选择打印成功或者失败的消息 (default [failed,succeed])
  -u, --user string                     远程ssh用户,默认当前用户
```
---
# 5. 拉取文件
```text
[root@duduniao ~]# gosh pull --help
批量拉取远程主机文件到本地，最多支持使用一层跳板机。
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提:
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配
4. 需要注意文件的访问权限问题

Examples:
# 拉取远程主机的文件到本地当前目录下，程序会在当前目录下创建一个以远程主机IP地址为名称的目录
~ gosh pull -i ip.txt /etc/nginx/conf.d /etc/nginx/nginx.conf ./

# 拉取远程主机上的错误日志到本地logs目录下
~ gosh pull -i ip.txt /var/log/nginx/error.log logs/

Usage:
  gosh pull {-i hosts.txt|-H host01,host02,...} [flags] remoteFile|remoteDir... LocalDirectory

Flags:
      --bastion.host string             指定跳板机IP
      --bastion.key string              指定跳板机ssh私钥
      --bastion.key.passphrase string   指定跳板机ssh私钥的密码
      --bastion.password string         指定跳板机密码
      --bastion.port int                指定跳板机端口
      --bastion.user string             指定跳板机用户,默认当前用户
  -t, --connect.timeout duration        ssh连接超时时间 (default 5s)
  -f, --fork int                        指定并发数量 (default 10)
  -h, --help                            help for pull
  -H, --host string                     指定主机列表，使用逗号分隔
  -i, --inventory string                指定主机清单列表文件,# 开头为注释
  -k, --key string                      指定ssh私钥
      --key.passphrase string           ssh私钥的密码
  -l, --label strings                   指定标签选择器
  -P, --password string                 远程ssh用户的密码
  -p, --port int                        指定远程主机ssh的端口
      --print.field strings             选择打印的消息字段 (default [ip,stdout,stderr])
      --print.status strings            选择打印成功或者失败的消息 (default [failed,succeed])
  -u, --user string                     远程ssh用户,默认当前用户
```

---
# 6. 端口转发
```text
[root@duduniao ~]# gosh proxy --help
将远程服务TCP端口代理到本地,方便服务调测。使用ssh工具实现本地端口转发和远程端口转发
可参考文档：https://www.yuque.com/duduniao/linux/nwsn6p#Kt3rn

Usage:
  gosh proxy -H host [flags] [-l local] -r remote

Flags:
  -j, --bastion.host string             指定跳板机IP
      --bastion.key string              指定跳板机ssh私钥 (default "~/.ssh/id_rsa")
      --bastion.key.passphrase string   指定跳板机ssh私钥的密码
      --bastion.password string         指定跳板机密码
      --bastion.port int                指定跳板机SSH端口 (default 22)
      --bastion.user string             指定跳板机SSH用户,默认当前用户
  -h, --help                            help for proxy
  -H, --host string                     转发请求的远程主机
  -k, --key string                      指定ssh私钥 (default "~/.ssh/id_rsa")
      --key.passphrase string           ssh私钥的密码
  -l, --local string                    本地监听端口 (default "127.0.0.1:10080")
  -P, --password string                 远程ssh用户的密码
  -p, --port int                        指定远程主机ssh的端口 (default 22)
      --protocol string                 远程服务器的通宵协议,tcp,tcp4,tcp6,unix (default "tcp4")
  -r, --remote string                   目标服务地址
  -t, --timeout duration                连接超时时间,ssh连接和proxy转发超时 (default 5s)
      --type string                     指定端口转发类型，local:本地端口转发，remote:远程端口转发 (default "local")
  -u, --user string                     远程ssh用户,默认当前用户
```

# 7. 登录服务器并打开交互式终端
```text
[root@duduniao ~]# gosh login --help
使用ssh协议登录远程服务器，最多支持使用一层跳板机
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提:
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配

Examples:
# 登录 10.100.100.101
~ gosh login -H 10.100.100.101

# 使用 ops 帐号并通过 2223 端口登录到 10.100.100.101
~ gosh login -H 10.100.100.101 -u ops -p 2333

Usage:
  gosh login -H host [flags]

Flags:
  -j, --bastion.host string             指定跳板机IP
      --bastion.key string              指定跳板机ssh私钥
      --bastion.key.passphrase string   指定跳板机ssh私钥的密码
      --bastion.password string         指定跳板机密码
      --bastion.port int                指定跳板机SSH端口
      --bastion.user string             指定跳板机SSH用户,默认当前用户
  -h, --help                            help for login
  -H, --host string                     需要登录的远程主机
  -k, --key string                      指定ssh私钥
      --key.passphrase string           ssh私钥的密码
  -l, --label strings                   指定标签选择器
  -P, --password string                 远程ssh用户的密码
  -p, --port int                        指定远程主机ssh的端口
  -t, --timeout duration                连接超时时间,ssh连接和proxy转发超时 (default 5s)
  -u, --user string                     远程ssh用户,默认当前用户
```
