package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gosh/pkg/login"
	"os"
	"time"
)

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login -H host [flags]",
	Short: "使用ssh协议登录远程服务器",
	Long: `使用ssh协议登录远程服务器，最多支持使用一层跳板机
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提: 
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user 
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配

Examples:
# 登录 10.100.100.101 
~ gosh login -H 10.100.100.101 

# 使用 ops 帐号并通过 2223 端口登录到 10.100.100.101
~ gosh login -H 10.100.100.101 -u ops -p 2333

`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if err := login.LoginConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		login.Run()
	},
}

func init() {
	rootCmd.AddCommand(loginCmd)
	loginCmd.Flags().StringVarP(&login.LoginConfig.Host, "host", "H", "", "需要登录的远程主机")
	loginCmd.Flags().IntVarP(&login.LoginConfig.Port, "port", "p", 0, "指定远程主机ssh的端口")
	loginCmd.Flags().StringVarP(&login.LoginConfig.User, "user", "u", "", "远程ssh用户,默认当前用户")
	loginCmd.Flags().StringVarP(&login.LoginConfig.Password, "password", "P", "", "远程ssh用户的密码")
	loginCmd.Flags().StringVarP(&login.LoginConfig.Key, "key", "k", "", "指定ssh私钥")
	loginCmd.Flags().StringVarP(&login.LoginConfig.KeyPassphrase, "key.passphrase", "", "", "ssh私钥的密码")
	loginCmd.Flags().StringVarP(&login.LoginConfig.BastionHost, "bastion.host", "j", "", "指定跳板机IP")
	loginCmd.Flags().IntVarP(&login.LoginConfig.BastionPort, "bastion.port", "", 0, "指定跳板机SSH端口")
	loginCmd.Flags().StringVarP(&login.LoginConfig.BastionUser, "bastion.user", "", "", "指定跳板机SSH用户,默认当前用户")
	loginCmd.Flags().StringVarP(&login.LoginConfig.BastionPassword, "bastion.password", "", "", "指定跳板机密码")
	loginCmd.Flags().StringVarP(&login.LoginConfig.BastionKey, "bastion.key", "", "", "指定跳板机ssh私钥")
	loginCmd.Flags().StringVarP(&login.LoginConfig.BastionKeyPassphrase, "bastion.key.passphrase", "", "", "指定跳板机ssh私钥的密码")
	loginCmd.Flags().StringSliceVarP(&login.LoginConfig.Labels, "label", "l", []string{}, "指定标签选择器")
	loginCmd.Flags().DurationVarP(&login.LoginConfig.ConnectTimeout, "timeout", "t", time.Second*5, "连接超时时间,ssh连接和proxy转发超时")
	_ = loginCmd.MarkFlagRequired("host")
}
