// Package cmd /*
package cmd

import (
	"fmt"
	"gosh/config"
	"gosh/pkg/inventory"
	"os"

	"github.com/spf13/cobra"
)

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import -f <path>",
	Short: "批量导入主机和跳板机信息，如果数据库存在则更新，不存在则创建",
	Long: `执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

自动读取名为 bastion 的sheet作为 bastion 数据源，并跳过第一行(默认为title)
自动读取名为 host 的sheet作为 host 数据源，并跳过第一行(默认为title)

Examples:
# 从excel文件导入跳板机和服务器信息
gosh inventory -f /mnt/c/Users/test/Downloads/gosh.xlsx

`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if !config.GlobalConfig.UsedDB {
			fmt.Println("Environment GOSH_DB_USED is not true")
			os.Exit(1)
		}
		if err := inventory.ImportConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		inventory.ImportToDB()
	},
}

func init() {
	inventoryCmd.AddCommand(importCmd)
	importCmd.Flags().StringVarP(&inventory.ImportConfig.Path, "file", "f", "", "导入的文件")
	_ = importCmd.MarkFlagRequired("file")
}
