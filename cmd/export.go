/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"gosh/config"
	"gosh/pkg/inventory"
	"os"

	"github.com/spf13/cobra"
)

// exportCmd represents the export command
var exportCmd = &cobra.Command{
	Use:   "export -f <path> [-t host|bastion|all] [-l k1=v1] [-l k2=v2] ...",
	Short: "根据条件导出本地主机信息",
	Long: `执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

Note: 因为考虑到性能问题，不支持 模糊匹配, 取反等操作。
      当主机信息发生变更时，可以先导入这部分机器，然后在 excel 中修改完毕后再导入

Examples:
# 导出全部信息:
~ gosh inventory export -f res.xlsx

# 导出腾讯上海主机信息:
~ gosh inventory export -t host -l vendor=tencent -l region=shanghai -f res.xlsx

# 查询指定IP的跳板机
~ gosh inventory export -t bastion -l private_ip=127.0.0.1 -f res.xlsx

`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if !config.GlobalConfig.UsedDB {
			fmt.Println("Environment GOSH_DB_USED is not true")
			os.Exit(1)
		}
		if err := inventory.ExportConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		inventory.ExportFromDB()
	},
}

func init() {
	inventoryCmd.AddCommand(exportCmd)
	exportCmd.Flags().StringVarP(&inventory.ExportConfig.Filter.Type, "type", "t", "all", "导入的数据类型,支持bastion,host,all")
	exportCmd.Flags().StringSliceVarP(&inventory.ExportConfig.Filter.Label, "label", "l", []string{}, "查询条件")
	exportCmd.Flags().StringVarP(&inventory.ExportConfig.Path, "file", "f", "gosh.xlsx", "导出的文件路径")
	_ = exportCmd.MarkFlagRequired("file")
}
