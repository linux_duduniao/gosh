// Package cmd /*
package cmd

import (
	"fmt"
	"gosh/pkg/login"
	"gosh/pkg/shell"
	"os"
	"time"

	"github.com/spf13/cobra"
)

// pushCmd represents the push command
var pullCmd = &cobra.Command{
	Use:   "pull {-i hosts.txt|-H host01,host02,...} [flags] remoteFile|remoteDir... LocalDirectory",
	Short: "批量拉取远程主机文件到本地",
	Long: `批量拉取远程主机文件到本地，最多支持使用一层跳板机。
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提: 
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user 
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配
4. 需要注意文件的访问权限问题

Examples:
# 拉取远程主机的文件到本地当前目录下，程序会在当前目录下创建一个以远程主机IP地址为名称的目录
~ gosh pull -i ip.txt /etc/nginx/conf.d /etc/nginx/nginx.conf ./

# 拉取远程主机上的错误日志到本地logs目录下
~ gosh pull -i ip.txt /var/log/nginx/error.log logs/

`,
	Args: cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		shell.SSHConfig.Args = args
		if err := shell.SSHConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		shell.PullFiles()
	},
}

func init() {
	rootCmd.AddCommand(pullCmd)
	pullCmd.Flags().StringVarP(&shell.SSHConfig.Inventory, "inventory", "i", "", "指定主机清单列表文件,# 开头为注释")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.Host, "host", "H", "", "指定主机列表，使用逗号分隔")
	pullCmd.Flags().IntVarP(&shell.SSHConfig.Port, "port", "p", 0, "指定远程主机ssh的端口")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.User, "user", "u", "", "远程ssh用户,默认当前用户")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.Password, "password", "P", "", "远程ssh用户的密码")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.Key, "key", "k", "", "指定ssh私钥")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.KeyPassphrase, "key.passphrase", "", "", "ssh私钥的密码")
	pullCmd.Flags().IntVarP(&shell.SSHConfig.Fork, "fork", "f", 10, "指定并发数量")
	pullCmd.Flags().StringSliceVarP(&login.LoginConfig.Labels, "label", "l", []string{}, "指定标签选择器")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.BastionHost, "bastion.host", "", "", "指定跳板机IP")
	pullCmd.Flags().IntVarP(&shell.SSHConfig.BastionPort, "bastion.port", "", 0, "指定跳板机端口")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.BastionUser, "bastion.user", "", "", "指定跳板机用户,默认当前用户")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.BastionPassword, "bastion.password", "", "", "指定跳板机密码")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.BastionKey, "bastion.key", "", "", "指定跳板机ssh私钥")
	pullCmd.Flags().StringVarP(&shell.SSHConfig.BastionKeyPassphrase, "bastion.key.passphrase", "", "", "指定跳板机ssh私钥的密码")
	pullCmd.Flags().DurationVarP(&shell.SSHConfig.ConnectTimeout, "connect.timeout", "t", time.Second*5, "ssh连接超时时间")
	pullCmd.Flags().StringSliceVarP(&shell.SSHConfig.PrintFields, "print.field", "", []string{"ip", "stdout", "stderr"}, "选择打印的消息字段")
	pullCmd.Flags().StringSliceVarP(&shell.SSHConfig.PrintStatus, "print.status", "", []string{"failed", "succeed"}, "选择打印成功或者失败的消息")
}
