/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"
	"gosh/config"
	"gosh/pkg/inventory"
	"os"

	"github.com/spf13/cobra"
)

// deleteCmd represents the delete command
var deleteCmd = &cobra.Command{
	Use:   "delete [-t host|bastion] [-l k1=v1] [-l k2=v2]",
	Short: "批量删除数据库中的主机数据",
	Long: `执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

Note: 因为考虑到性能问题，不支持 模糊匹配。为了避免误操作清空数据库，要求必须填写条件

Examples:

# 删除腾讯上海的服务器:
~ gosh inventory delete -t host -l region=shanghai -l vendor=tencent

# 删除指定IP的跳板机
~ gosh inventory delete -t bastion -l private_ip=127.0.0.1

`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if !config.GlobalConfig.UsedDB {
			fmt.Println("Environment GOSH_DB_USED is not true")
			os.Exit(1)
		}
		if err := inventory.FilterConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		inventory.Delete()
	},
}

func init() {
	inventoryCmd.AddCommand(deleteCmd)
	deleteCmd.Flags().StringVarP(&inventory.FilterConfig.Type, "type", "t", "host", "删除的数据类型,支持bastion和host")
	deleteCmd.Flags().StringSliceVarP(&inventory.FilterConfig.Label, "label", "l", []string{}, "查询条件")
	_ = deleteCmd.MarkFlagRequired("label")
}
