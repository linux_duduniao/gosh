// Package cmd /*
package cmd

import (
	"fmt"
	"gosh/config"
	"gosh/pkg/inventory"
	"os"

	"github.com/spf13/cobra"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get [-t host|bastion] [-l k1=v1] [-l k2=v2] ... ",
	Short: "根据条件查询本地主机信息",
	Long: `执行该命令必须要提前设置环境变量:
GOSH_DB_USED=true       # 启用数据库
GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db

Note: 因为考虑到性能问题，不支持 模糊匹配, 取反等操作

Examples:
# 查询全部跳板机信息:
~ gosh inventory get -t bastion 

# 查询腾讯上海跳板机信息:
~ gosh inventory get -t bastion -l vendor=tencent -l region=shanghai

# 查询指定IP的跳板机
~ gosh inventory get -t bastion -l private_ip=127.0.0.1

`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if !config.GlobalConfig.UsedDB {
			fmt.Println("Environment GOSH_DB_USED is not true")
			os.Exit(1)
		}
		if err := inventory.FilterConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		inventory.Query()
	},
}

func init() {
	inventoryCmd.AddCommand(getCmd)
	getCmd.Flags().StringVarP(&inventory.FilterConfig.Type, "type", "t", "host", "导入的数据类型,支持bastion和host")
	getCmd.Flags().StringSliceVarP(&inventory.FilterConfig.Label, "label", "l", []string{}, "查询条件")
}
