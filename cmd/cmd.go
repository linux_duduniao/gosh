// Package cmd /*
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gosh/pkg/shell"
	"os"
	"time"
)

// cmdCmd represents the cmd command
var cmdCmd = &cobra.Command{
	Use:   "cmd {-i hosts.txt|-H host01,host02,...} [flags] commands...",
	Short: "连接远程服务器并执行shell命令",
	Long: `批量连接远程服务器并执行shell命令，最多支持使用一层跳板机
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提:
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配

Examples:
# 输出无法连通的机器IP地址
~ gosh cmd -i ip.txt -u ops -p 2333 --print.status failed --print.field ip "id"

# 读取 ip.txt 文件中的列表，并检查主机名, 提升至30并发:
~ gosh cmd -i ip.txt -f 30 "hostname" | xargs -n 2 | column -t

# 检查服务状态
~ gosh cmd -i ip.txt "sudo systemctl is-active firewalld.service; sudo systemctl is-enabled firewalld.service"| xargs -n 3 | column -t

# 远程执行脚本，并打印执行失败的主机执行信息
~ gosh cmd -i ip.txt --print.status failed "sudo bash /tmp/install.sh"

# 读取 ip.txt 文件中的列表，并远程扫描 /data 数据目录大小:
~ gosh cmd -i ip.txt -u ops -p 2333 -k ~/.ssh/k1 --bastion.host 10.100.100.100 "df -h /data/ | awk 'NR==2{print \$2}'"

# 从命令行直接读取IP列表，可以使用 echo 'xxx xxx xxx' | xargs | tr " " "," 生成列表
~ gosh cmd -H 127.0.0.1,127.0.0.2,127.0.0.3,127.0.0.4,127.0.0.5 "grep '^#PermitRootLogin' /etc/ssh/sshd_config"

`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		shell.SSHConfig.Args = args
		if err := shell.SSHConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		shell.ExecuteCommand()
	},
}

func init() {
	rootCmd.AddCommand(cmdCmd)
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.Inventory, "inventory", "i", "", "指定主机清单列表文件,# 开头为注释")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.Host, "host", "H", "", "指定主机列表，使用逗号分隔")
	cmdCmd.Flags().IntVarP(&shell.SSHConfig.Port, "port", "p", 0, "指定远程主机ssh的端口")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.User, "user", "u", "", "远程ssh用户,默认当前用户")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.Password, "password", "P", "", "远程ssh用户的密码")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.Key, "key", "k", "", "指定ssh私钥")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.KeyPassphrase, "key.passphrase", "", "", "ssh私钥的密码")
	cmdCmd.Flags().IntVarP(&shell.SSHConfig.Fork, "fork", "f", 10, "指定并发数量")
	cmdCmd.Flags().StringSliceVarP(&shell.SSHConfig.Labels, "label", "l", []string{}, "指定标签选择器")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.BastionHost, "bastion.host", "", "", "指定跳板机IP")
	cmdCmd.Flags().IntVarP(&shell.SSHConfig.BastionPort, "bastion.port", "", 0, "指定跳板机端口")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.BastionUser, "bastion.user", "", "", "指定跳板机用户,默认当前用户")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.BastionPassword, "bastion.password", "", "", "指定跳板机密码")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.BastionKey, "bastion.key", "", "", "指定跳板机ssh私钥")
	cmdCmd.Flags().StringVarP(&shell.SSHConfig.BastionKeyPassphrase, "bastion.key.passphrase", "", "", "指定跳板机ssh私钥的密码")
	cmdCmd.Flags().DurationVarP(&shell.SSHConfig.ConnectTimeout, "connect.timeout", "t", time.Second*5, "ssh连接超时时间")
	cmdCmd.Flags().StringSliceVarP(&shell.SSHConfig.PrintFields, "print.field", "", []string{"ip", "stdout", "stderr"}, "选择打印的消息字段")
	cmdCmd.Flags().StringSliceVarP(&shell.SSHConfig.PrintStatus, "print.status", "", []string{"failed", "succeed"}, "选择打印成功或者失败的消息")
}
