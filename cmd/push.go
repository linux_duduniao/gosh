// Package cmd /*
package cmd

import (
	"fmt"
	"gosh/pkg/login"
	"gosh/pkg/shell"
	"os"
	"time"

	"github.com/spf13/cobra"
)

// pushCmd represents the push command
var pushCmd = &cobra.Command{
	Use:   "push {-i hosts.txt|-H host01,host02,...} [flags] localFile... remoteDirectory",
	Short: "批量发送文件到远程主机",
	Long: `批量发送文件到远程主机，最多支持使用一层跳板机。
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提: 
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user 
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配 
   虚拟机支持的筛选字段: vendor,region,private_ip,public_ip,user,port,key,bastion,connect_ip
4. push 命令传输速度很慢，不适合传输大文件，速度远低于 scp 命令
5. 将本地目录传输到远程时，如果本地目录以 / 结尾，则拷贝目录下子文件

Examples:
# 读取 ip.txt 文件中的列表，下发系统参数配置到远程主机上:
~ gosh push -i ip.txt ops-sysctl.conf /etc/sysctl.d/

# 读取 ip.txt 文件中的列表，并推送nginx配置到远程主机:
~ gosh push -i ip.txt -u ops -p 2333 -k ~/.ssh/k1 --bastion.host 10.100.100.100 nginx.conf conf.d ssl /etc/nginx/

# 读取 ip.txt 文件中的列表，并推送压缩文件到远程主机:
~ gosh push -i ip.txt -u ops -p 2333 tarball/*.tar.gz /opt/src/

# 读取 ip.txt 文件中的列表，并推送filebeat目录下配置文件到远程主机:
~ gosh push -i ip.txt -u ops -p 2333 config/filebeat/ /opt/apps/filebeat/inputs.d/

`,
	Args: cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		shell.SSHConfig.Args = args
		if err := shell.SSHConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		shell.PushFiles()
	},
}

func init() {
	rootCmd.AddCommand(pushCmd)
	pushCmd.Flags().StringVarP(&shell.SSHConfig.Inventory, "inventory", "i", "", "指定主机清单列表文件,# 开头为注释")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.Host, "host", "H", "", "指定主机列表，使用逗号分隔")
	pushCmd.Flags().IntVarP(&shell.SSHConfig.Port, "port", "p", 0, "指定远程主机ssh的端口")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.User, "user", "u", "", "远程ssh用户,默认当前用户")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.Password, "password", "P", "", "远程ssh用户的密码")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.Key, "key", "k", "", "指定ssh私钥")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.KeyPassphrase, "key.passphrase", "", "", "ssh私钥的密码")
	pushCmd.Flags().IntVarP(&shell.SSHConfig.Fork, "fork", "f", 10, "指定并发数量")
	pushCmd.Flags().StringSliceVarP(&login.LoginConfig.Labels, "label", "l", []string{}, "指定标签选择器")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.BastionHost, "bastion.host", "", "", "指定跳板机IP")
	pushCmd.Flags().IntVarP(&shell.SSHConfig.BastionPort, "bastion.port", "", 0, "指定跳板机端口")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.BastionUser, "bastion.user", "", "", "指定跳板机用户,默认当前用户")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.BastionPassword, "bastion.password", "", "", "指定跳板机密码")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.BastionKey, "bastion.key", "", "", "指定跳板机ssh私钥")
	pushCmd.Flags().StringVarP(&shell.SSHConfig.BastionKeyPassphrase, "bastion.key.passphrase", "", "", "指定跳板机ssh私钥的密码")
	pushCmd.Flags().DurationVarP(&shell.SSHConfig.ConnectTimeout, "connect.timeout", "t", time.Second*5, "ssh连接超时时间")
	pushCmd.Flags().StringSliceVarP(&shell.SSHConfig.PrintFields, "print.field", "", []string{"ip", "stdout", "stderr"}, "选择打印的消息字段")
	pushCmd.Flags().StringSliceVarP(&shell.SSHConfig.PrintStatus, "print.status", "", []string{"failed", "succeed"}, "选择打印成功或者失败的消息")
}
