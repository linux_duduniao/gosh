package cmd

import (
	"fmt"
	"gosh/pkg/proxy"
	"os"
	"time"

	"github.com/spf13/cobra"
)

// proxyCmd represents the proxy command
var proxyCmd = &cobra.Command{
	Use:   "proxy -H host [flags] [-l local] -r remote",
	Short: "将远程服务TCP端口代理到本地",
	Long: `将远程服务TCP端口通过ssh协议代理到本地，通常用于服务调测，最多支持使用一层跳板机
可以从本地数据库读取主机连接信息，比如 ssh_user, ssh_port, bastion_ip 等，减少每次传递参数的繁琐流程,使用数据库前提: 
1. 设置数据库环境变量
   GOSH_DB_USED=true       # 启用数据库
   GOSH_DB_FILE=xxx.db     # 指定SQLite数据库存放路径，默认: ~/.config/gosh.db
2. 使用 gosh inventory import 导入数据

Note:
1. 优先级: 命令行 > 数据库，比如命令行传递 --user root，会覆盖数据库中的 ssh_user 
2. 默认值：如果命令行没有指定，数据库也没指定，那么以下字段使用默认值:
          user/bastion.user: 当前运行gosh的用户名
          port/bastion.port: 22
          key/bastion.key: ~/.ssh/id_rsa
3. 为了避免特殊情况下，数据库中同一个 ssh_ip 有多条记录(不同地区采用的私有网络地址重复)，可使用 --label 筛选，取第一个匹配数据
   因为考虑到性能问题，不支持模糊匹配
4. 关于 ssh 转发可参考文档：https://www.yuque.com/duduniao/linux/nwsn6p#Kt3rn

Examples:
# 将远程Prometheus的 9090 端口映射到本地方便调试
~ gosh proxy -H 10.100.100.100 -l 0.0.0.0:9090 -r 127.0.0.1:9090

# 将本地开发的服务端口映射到远程服务器上，方便同事临时测试
  远程端口转发需要在远程服务器上的 /etc/ssh/sshd_config 开启 GatewayPorts yes 
~ gosh proxy -H 10.100.100.100 --type remote -l 127.0.0.1:10086 -r 0.0.0.0:10086 

`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if err := proxy.ProxyConfig.Parse(); err != nil {
			fmt.Println(err.Error())
			os.Exit(1)
		}
		proxy.Run()
	},
}

func init() {
	rootCmd.AddCommand(proxyCmd)
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.Host, "host", "H", "", "转发请求的远程主机")
	proxyCmd.Flags().IntVarP(&proxy.ProxyConfig.Port, "port", "p", 0, "指定远程主机ssh的端口")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.User, "user", "u", "", "远程ssh用户,默认当前用户")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.Password, "password", "P", "", "远程ssh用户的密码")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.Key, "key", "k", "", "指定ssh私钥")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.KeyPassphrase, "key.passphrase", "", "", "ssh私钥的密码")
	proxyCmd.Flags().StringSliceVarP(&proxy.ProxyConfig.Labels, "label", "", []string{}, "指定标签选择器")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.BastionHost, "bastion.host", "j", "", "指定跳板机IP")
	proxyCmd.Flags().IntVarP(&proxy.ProxyConfig.BastionPort, "bastion.port", "", 0, "指定跳板机SSH端口")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.BastionUser, "bastion.user", "", "", "指定跳板机SSH用户,默认当前用户")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.BastionPassword, "bastion.password", "", "", "指定跳板机密码")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.BastionKey, "bastion.key", "", "", "指定跳板机ssh私钥")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.BastionKeyPassphrase, "bastion.key.passphrase", "", "", "指定跳板机ssh私钥的密码")
	proxyCmd.Flags().DurationVarP(&proxy.ProxyConfig.ConnectTimeout, "timeout", "t", time.Second*5, "连接超时时间,ssh连接和proxy转发超时")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.LocalAddress, "local", "l", "127.0.0.1:10080", "本地监听端口")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.RemoteAddress, "remote", "r", "", "目标服务地址")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.Protocol, "protocol", "", "tcp4", "远程服务器的通宵协议,tcp,tcp4,tcp6,unix")
	proxyCmd.Flags().StringVarP(&proxy.ProxyConfig.Type, "type", "", "local", "指定端口转发类型，local:本地端口转发，remote:远程端口转发")
	_ = proxyCmd.MarkFlagRequired("remote")
}
