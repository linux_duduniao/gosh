module gosh

go 1.17

require (
	github.com/fatih/color v1.13.0
	github.com/liushuochen/gotable v0.0.0-20220831134725-cbcd6bb0a5f9
	github.com/pkg/sftp v1.13.4
	github.com/spf13/cobra v1.4.0
	github.com/xuri/excelize/v2 v2.7.0
	golang.org/x/crypto v0.5.0
	gorm.io/driver/sqlite v1.4.3
	gorm.io/gorm v1.24.0
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-sqlite3 v1.14.15 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/richardlehane/mscfb v1.0.4 // indirect
	github.com/richardlehane/msoleps v1.0.3 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/xuri/efp v0.0.0-20220603152613-6918739fd470 // indirect
	github.com/xuri/nfp v0.0.0-20220409054826-5e722a1d9e22 // indirect
	golang.org/x/net v0.5.0 // indirect
	golang.org/x/sys v0.4.0 // indirect
	golang.org/x/term v0.4.0 // indirect
	golang.org/x/text v0.6.0 // indirect
)
